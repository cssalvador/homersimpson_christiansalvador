package com.example.homersimpson_christiansalvador;

import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ImageView animacio;
    ImageView eng_vermell, eng_blau, eng_verd,im_ull, img_donut;
    MediaPlayer media;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        animacio = findViewById(R.id.imgTitol);
        eng_vermell = findViewById(R.id.img_engranatge_vermell);
        eng_blau = findViewById(R.id.img_engranatge_blau);
        eng_verd = findViewById(R.id.img_engranatge_verd);
        im_ull = findViewById(R.id.img_ull);
        img_donut = findViewById(R.id.img_donut);

        img_donut.setVisibility(View.VISIBLE);
        eng_vermell.setVisibility(View.INVISIBLE);
        eng_blau.setVisibility(View.INVISIBLE);
        eng_verd.setVisibility(View.INVISIBLE);
        im_ull.setVisibility(View.INVISIBLE);
        animaciotitol();

        img_donut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (media != null && media.isPlaying())
                {
                    media.stop();
                }
                else
                {
                    media = MediaPlayer.create(getApplication().getApplicationContext(), R.raw.the_simpsons);
                    media.start();
                }
            }
        });

        animacio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Carrega l'animació de la carpeta /res/anim
                Animation anim = AnimationUtils.loadAnimation(v.getContext(), R.anim.engranatge_vermell);
                Animation anim2 = AnimationUtils.loadAnimation(v.getContext(), R.anim.engranatge_blau);
                Animation anim3 = AnimationUtils.loadAnimation(v.getContext(), R.anim.engranatge_verd);
                Animation anim4 = AnimationUtils.loadAnimation(v.getContext(), R.anim.ull);
                Animation anim5 = AnimationUtils.loadAnimation(v.getContext(), R.anim.donut);
                //Inicia l'animació cada cop que fem click.
                eng_vermell.startAnimation(anim);
                if (anim.isInitialized()) {
                    eng_vermell.setVisibility(View.VISIBLE);
                }
                else{
                    eng_vermell.setVisibility(View.INVISIBLE);
                }
                eng_blau.startAnimation(anim2);
                if (anim2.isInitialized()) {
                    eng_blau.setVisibility(View.VISIBLE);
                }
                else{
                    eng_blau.setVisibility(View.INVISIBLE);
                }
                eng_verd.startAnimation(anim3);
                if (anim3.isInitialized()) {
                    eng_verd.setVisibility(View.VISIBLE);
                }
                else{
                    eng_verd.setVisibility(View.INVISIBLE);
                }
                im_ull.startAnimation(anim4);
                if (anim4.isInitialized()) {
                    im_ull.setVisibility(View.VISIBLE);
                }
                else{
                    im_ull.setVisibility(View.INVISIBLE);
                }
                img_donut.startAnimation(anim5);

            }
        });

    }

    public void animaciotitol() {
        AnimationDrawable anim = (AnimationDrawable) animacio.getDrawable();

        //Inicia o para l'animació
        if (anim.isRunning())
            anim.stop();
        else
            anim.start();
    }
}
